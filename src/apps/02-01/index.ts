import * as fs from "fs";
import path from "path";
import { FuelCounter } from "../../lib/fuel-counter";
import { IntCodeInterpreter } from "../../lib/int-code-interpreter";

function main() {
  const filePath = path.join(__dirname, 'input.txt');
  const program = fs.readFileSync(filePath, 'utf-8');

  const interpreter = new IntCodeInterpreter();
  interpreter.load(program);

  interpreter.overwrite(1, 12);
  interpreter.overwrite(2, 2);

  interpreter.run();

  const currentMemory = interpreter.dumpMemory();

  console.log('Current memory:');
  console.log(currentMemory);
}

main();
