import { expect } from "chai";
import { InputReader, IntCodeInterpreter, OutputWriter } from "../src/lib/int-code-interpreter";

class InputReaderStub implements InputReader {
  private stubbedValue = -1;

  setup(value: number) {
    this.stubbedValue = value;
  }

  read(): number {
    return this.stubbedValue;
  }
}

class OutputWriterMock implements OutputWriter {
  readonly writes: number[] = [];

  write(output: number): void {
    this.writes.push(output);
  }
}

describe('the int code interpreter', () => {
  let interpreter: IntCodeInterpreter;
  let readerStub: InputReaderStub;
  let outputWriterMock: OutputWriterMock;

  beforeEach(() => {
    readerStub = new InputReaderStub();
    outputWriterMock = new OutputWriterMock();
    interpreter = new IntCodeInterpreter(readerStub, outputWriterMock);
  });

  describe('instructions', () => {
    describe('01: addition', () => {
      it('supports two position parameters', () => {
        const input = "1,5,6,7,99,3,4,0";

        interpreter.load(input);
        interpreter.run();

        expect(interpreter.dumpMemory()).to.equal("1,5,6,7,99,3,4,7");
      });

      it('supports two immediate parameters', () => {
        const program = "1101,5,6,5," +
          "99," +
          "0";

        interpreter.load(program);
        interpreter.run();

        expect(interpreter.dumpMemory()).to.equal("1101,5,6,5,99,11");
      });

      it('supports a mix of immediate and positional parameters', () => {
        const program = "1001,5,10,6," +
          "99," +
          "3,0";

        const expectedState = "1001,5,10,6," +
          "99," +
          "3,13";

        interpreter.load(program);
        interpreter.run();

        expect(interpreter.dumpMemory()).to.equal(expectedState);
      });
    });

    describe('02: multiplication', () => {
      it('supports two position parameters', () => {
        const input = "2,5,6,7,99,3,4,0";

        interpreter.load(input);
        interpreter.run();

        expect(interpreter.dumpMemory()).to.equal("2,5,6,7,99,3,4,12");
      });

      it('supports two immediate parameters', () => {
        const program = "1102,5,6,5," +
          "99," +
          "0";

        interpreter.load(program);
        interpreter.run();

        expect(interpreter.dumpMemory()).to.equal("1102,5,6,5,99,30");
      });

      it('supports a mix of immediate and positional parameters', () => {
        const program = "1002,5,10,6," +
          "99," +
          "3,0";

        const expectedState = "1002,5,10,6," +
          "99," +
          "3,30";

        interpreter.load(program);
        interpreter.run();

        expect(interpreter.dumpMemory()).to.equal(expectedState);
      });
    });

    describe('03: input', () => {
      beforeEach(() => {
        readerStub.setup(123);
      });

      it('assigns the inputted integer into memory', () => {
        const program =
          "3,3," +
          "99," +
          "0";

        interpreter.load(program);
        interpreter.run();

        expect(interpreter.dumpMemory()).to.equal("3,3,99,123");
      });

      it('only advances the program two instructions', () => {
        const program =
          "3,7," +
          "1,7,8,9," +
          "99," +
          "0,1,0";

        const expectedState =
          "3,7," +
          "1,7,8,9," +
          "99," +
          "123,1,124";

        interpreter.load(program);
        interpreter.run();

        expect(interpreter.dumpMemory()).to.equal(expectedState);
      });

      it('is an unsupported operator if no input reader provided', () => {
        const interpreterMissingDependencies = new IntCodeInterpreter();

        const program =
          "3,3," +
          "99," +
          "0";

        interpreterMissingDependencies.load(program);
        expect(() => { interpreterMissingDependencies.run() }).to.throw();
      });
    });

    describe('04: output', () => {
      it('writes the memory address to output when provided a positional param', () => {
        const program =
          "4,3," +
          "99," +
          "123";

        interpreter.load(program);
        interpreter.run();

        expect(outputWriterMock.writes).to.eql([123]);
      });

      it('writes the value to output when provided a immediate param', () => {
        const program =
          "104,321," +
          "99," +
          "123";

        interpreter.load(program);
        interpreter.run();

        expect(outputWriterMock.writes).to.eql([321]);
      });

      it('only advances the program two instructions', () => {
        const program =
          "4,7," +
          "1,7,8,9," +
          "99," +
          "123,1,0";

        const expectedState =
          "4,7," +
          "1,7,8,9," +
          "99," +
          "123,1,124";

        interpreter.load(program);
        interpreter.run();

        expect(interpreter.dumpMemory()).to.equal(expectedState);
      });

      it('is an unsupported operator if no output writer provided', () => {
        const interpreterMissingDependencies = new IntCodeInterpreter();

        const program =
          "4,3," +
          "99," +
          "123";

        interpreterMissingDependencies.load(program);
        expect(() => { interpreterMissingDependencies.run() }).to.throw();
      });
    });

    describe('05: jump-if-true', () => {
      it('does nothing if memory[arg1] is zero', () => {
        const program =
          "5,8,9," +      // jump to mem[9] next if mem[8]
          "104, 123," +   // output 123
          "104, 234," +   // output 234
          "99," +         // end
          "0,5";          // mem[8], mem[9]

        interpreter.load(program);
        interpreter.run();

        expect(outputWriterMock.writes).to.eql([123, 234]);
      });

      it('jumps to the instruction if memory[arg1] is non-zero', () => {
        const program =
          "5,8,9," +      // jump to mem[9] next if mem[8]
          "104, 123," +   // output 123
          "104, 234," +   // output 234
          "99," +         // end
          "1,5";          // mem[8], mem[9]

        interpreter.load(program);
        interpreter.run();

        expect(outputWriterMock.writes).to.eql([234]);
      });

      it('does nothing if value[arg1] is zero', () => {
        const program =
          "1105,0,5," +   // jump to 5 next if 0
          "104, 123," +   // output 123
          "104, 234," +   // output 234
          "99";           // end

        interpreter.load(program);
        interpreter.run();

        expect(outputWriterMock.writes).to.eql([123, 234]);
      });

      it('jumps to the instruction if value[arg1] is non-zero', () => {
        const program =
          "1105,1,5," +   // jump to 5 next if 1
          "104, 123," +   // output 123
          "104, 234," +   // output 234
          "99";           // end

        interpreter.load(program);
        interpreter.run();

        expect(outputWriterMock.writes).to.eql([234]);
      });
    });

    describe('06: jump-if-false', () => {
      it('does nothing if memory[arg1] is non-zero', () => {
        const program =
          "6,8,9," +      // jump to mem[9] next if !mem[8]
          "104, 123," +   // output 123
          "104, 234," +   // output 234
          "99," +         // end
          "1,5";          // mem[8], mem[9]

        interpreter.load(program);
        interpreter.run();

        expect(outputWriterMock.writes).to.eql([123, 234]);
      });

      it('jumps to the instruction if memory[arg1] is zero', () => {
        const program =
          "6,8,9," +      // jump to mem[9] next if !mem[8]
          "104, 123," +   // output 123
          "104, 234," +   // output 234
          "99," +         // end
          "0,5";          // mem[8], mem[9]

        interpreter.load(program);
        interpreter.run();

        expect(outputWriterMock.writes).to.eql([234]);
      });

      it('does nothing if value[arg1] is non-zero', () => {
        const program =
          "1106,1,5," +   // jump to 5 next if !1
          "104, 123," +   // output 123
          "104, 234," +   // output 234
          "99";           // end

        interpreter.load(program);
        interpreter.run();

        expect(outputWriterMock.writes).to.eql([123, 234]);
      });

      it('jumps to the instruction if value[arg1] is zero', () => {
        const program =
          "1106,0,5," +   // jump to 5 next if !0
          "104, 123," +   // output 123
          "104, 234," +   // output 234
          "99";           // end

        interpreter.load(program);
        interpreter.run();

        expect(outputWriterMock.writes).to.eql([234]);
      });
    });

    describe('07: less than', () => {
      it('writes 1 when arg1 is less than arg2', () => {
        const program =
          "7,5,6,7," +
          "99," +
          "1,2,-1";

        const expectedState =
          "7,5,6,7," +
          "99," +
          "1,2,1";

        interpreter.load(program);
        interpreter.run();

        expect(interpreter.dumpMemory()).to.equal(expectedState);
      });

      it('writes 0 when arg1 is not less than arg2', () => {
        const program =
          "7,5,6,7," +
          "99," +
          "2,1,-1";

        const expectedState =
          "7,5,6,7," +
          "99," +
          "2,1,0";

        interpreter.load(program);
        interpreter.run();

        expect(interpreter.dumpMemory()).to.equal(expectedState);
      });

      it('supports immediate parameters', () => {
        const trueProgram =
          "1107,1,2,5," +
          "99," +
          "-1";

        const trueProgramExpectedState =
          "1107,1,2,5," +
          "99," +
          "1";

        interpreter.load(trueProgram);
        interpreter.run();

        expect(interpreter.dumpMemory()).to.equal(trueProgramExpectedState);

        const falseProgram =
          "1107,2,1,5," +
          "99," +
          "-1";

        const falseProgramExpectedState =
          "1107,2,1,5," +
          "99," +
          "0";

        interpreter.load(falseProgram);
        interpreter.run();

        expect(interpreter.dumpMemory()).to.equal(falseProgramExpectedState);
      });
    });

    describe('08: equal', () => {
      it('writes 1 when arg1 is equal to arg2', () => {
        const program =
          "8,5,6,7," +
          "99," +
          "1,1,-1";

        const expectedState =
          "8,5,6,7," +
          "99," +
          "1,1,1";

        interpreter.load(program);
        interpreter.run();

        expect(interpreter.dumpMemory()).to.equal(expectedState);
      });

      it('writes 0 when arg1 does not equal arg2', () => {
        const program =
          "8,5,6,7," +
          "99," +
          "1,2,-1";

        const expectedState =
          "8,5,6,7," +
          "99," +
          "1,2,0";

        interpreter.load(program);
        interpreter.run();

        expect(interpreter.dumpMemory()).to.equal(expectedState);
      });

      it('supports immediate parameters', () => {
        const trueProgram =
          "1108,40,40,5," +
          "99," +
          "-1";

        const trueProgramExpectedState =
          "1108,40,40,5," +
          "99," +
          "1";

        interpreter.load(trueProgram);
        interpreter.run();

        expect(interpreter.dumpMemory()).to.equal(trueProgramExpectedState);

        const falseProgram =
          "1108,40,41,5," +
          "99," +
          "-1";

        const falseProgramExpectedState =
          "1108,40,41,5," +
          "99," +
          "0";

        interpreter.load(falseProgram);
        interpreter.run();

        expect(interpreter.dumpMemory()).to.equal(falseProgramExpectedState);
      });
    });

    describe('99: end program', () => {
      it('does not execute further commands', () => {
        const input =
          "1,9,10,11," +
          "99," +
          "2,11,12,2," +
          "3,4,0,2,0";

        const expectedOutput =
          "1,9,10,11," +
          "99," +
          "2,11,12,2," +
          "3,4,7,2,0";

        interpreter.load(input);
        interpreter.run();

        expect(interpreter.dumpMemory()).to.equal(expectedOutput);
      });
    });
  });

  it('runs multiple operations', () => {
    const input =
      "1,9,10,11," +
      "2,11,12,13," +
      "99," +
      "3,4,0,2,0";

    const expectedOutput =
      "1,9,10,11," +
      "2,11,12,13," +
      "99," +
      "3,4,7,2,14";

    interpreter.load(input);
    interpreter.run();

    expect(interpreter.dumpMemory()).to.equal(expectedOutput);
  });

  it('allows for memory to be overwritten', () => {
    const input = "1,5,6,7,99,3,4,0";

    interpreter.load(input);

    interpreter.overwrite(5, 4);
    interpreter.overwrite(6, 5);

    interpreter.run();
    expect(interpreter.dumpMemory()).to.equal("1,5,6,7,99,4,5,9");
  });
});
