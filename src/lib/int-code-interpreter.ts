const MAX_INSTRUCTION_SIZE = 4;

export class IntCodeInterpreter {
  private memory: number[] = [];

  constructor(
    private reader: InputReader | null = null,
    private output: OutputWriter | null = null
  ) {}

  load(input: string) {
    this.memory = input.split(',').map(Number);
  }

  overwrite(position: number, value: number) {
    this.memory[position] = value;
  }

  dumpMemory() {
    return this.memory.join(',');
  }

  run() {
    let currentInstructionPosition = 0;

    while(currentInstructionPosition < this.memory.length) {
      const endPosition = currentInstructionPosition + MAX_INSTRUCTION_SIZE;
      const nextOperation = this.memory.slice(currentInstructionPosition, endPosition);
      const [instruction, arg1, arg2, arg3] = nextOperation;

      const opcode = extractOpCode(instruction);
      const value1 = this.loadArg(arg1, extractParamMode(instruction, 1));
      const value2 = this.loadArg(arg2, extractParamMode(instruction, 2));

      let nextInstruction = currentInstructionPosition + 4;

      switch (opcode) {
        case 1:
          this.doAddition(value1, value2, arg3);
          break;
        case 2:
          this.doMultiplication(value1, value2, arg3);
          break;
        case 3:
          this.doInput(arg1);
          nextInstruction = currentInstructionPosition + 2;
          break;
        case 4:
          this.doOutput(value1);
          nextInstruction = currentInstructionPosition + 2;
          break;
        case 5:
          nextInstruction =
            this.jumpIfTrue(value1, value2, currentInstructionPosition);
          break;
        case 6:
          nextInstruction =
            this.jumpIfFalse(value1, value2, currentInstructionPosition);
          break;
        case 7:
          this.doLessThan(value1, value2, arg3);
          break;
        case 8:
          this.doEqualTo(value1, value2, arg3);
          break;
        case 99:
          return;
      }

      currentInstructionPosition = nextInstruction;
    }
  }

  private loadArg(param: number, paramMode: ParamMode): number {
    switch (paramMode) {
      case ParamMode.Position:
        return this.memory[param];
      case ParamMode.Immediate:
        return param;
    }
  }

  private doAddition(x: number, y: number, outPosition: number) {
    this.memory[outPosition] = x + y;
  }

  private doMultiplication(x: number, y: number, outPosition: number) {
    this.memory[outPosition] = x * y;
  }

  private doInput(position: number) {
    if (this.reader === null) throw new MissingDependenciesError();
    this.memory[position] = this.reader.read();
  }

  private doOutput(value: number) {
    if (this.output === null) throw new MissingDependenciesError();
    this.output.write(value);
  }

  private doLessThan(x: number, y: number, outPosition: number) {
    const value = x < y ? 1 : 0;
    this.memory[outPosition] = value;
  }

  private doEqualTo(x: number, y: number, outPosition: number) {
    const value = x === y ? 1 : 0;
    this.memory[outPosition] = value;
  }

  private jumpIfTrue(x: number, jumpPosition: number, currentPosition: number) {
    return this.jumpIf(true, x, jumpPosition, currentPosition);
  }

  private jumpIfFalse(x: number, jumpPosition: number, currentPosition: number) {
    return this.jumpIf(false, x, jumpPosition, currentPosition);
  }

  private jumpIf(condition: boolean, x: number, jumpPosition: number, currentPosition: number) {
    if ((x !== 0) === condition)
      return jumpPosition;

    return currentPosition + 3;
  }
}

function extractOpCode(instruction: number): number {
  return Number(String(instruction).padStart(5, '0').substring(3, 5));
}

function extractParamMode(instruction: number, argNumber: number): ParamMode {
  const paramModeFlag = String(instruction).padStart(5, '0')[3 - argNumber];
  return paramModeFlag === '1' ? ParamMode.Immediate : ParamMode.Position;
}

enum ParamMode {
  Position,
  Immediate
}

export interface InputReader {
  read(): number;
}

export interface OutputWriter {
  write(output: number): void;
}

export class MissingDependenciesError extends Error {}
