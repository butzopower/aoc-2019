import * as fs from "fs";
import path from "path";
import { FuelCounter } from "../../lib/fuel-counter";
import { IntCodeInterpreter } from "../../lib/int-code-interpreter";

const filePath = path.join(__dirname, 'program.txt');
const program = fs.readFileSync(filePath, 'utf-8');

const EXPECTED_OUTPUT = 19690720;

function main() {
  const pairs: [number, number][] = Array.from(Array(100).keys())
    .flatMap(x => Array.from(Array(100).keys()).map<[number, number]>(y => [x, y]));

  const results = pairs.map(([x, y]) => {
    const output = runProgram(x, y);
    return {x, y, output};
  });

  const foundResult = results.find(({output}) => output === EXPECTED_OUTPUT);

  if (foundResult) {
    console.log(`Found inputs for result '${EXPECTED_OUTPUT}': [${foundResult.x},${foundResult.y}]`);
  } else {
    console.log('No results found.');
  }
}

function runProgram(arg1: number, arg2: number): number {
  const interpreter = new IntCodeInterpreter();
  interpreter.load(program);

  interpreter.overwrite(1, arg1);
  interpreter.overwrite(2, arg2);

  interpreter.run();

  const memory = interpreter.dumpMemory();

  return Number(memory.split(',')[0]);
}

main();
