export class FuelCounter {
  count(numbers: number[]) {
    return numbers
      .map(calculateTotalFuel)
      .reduce((x, y) => x + y);
  }
}
function calculateTotalFuel(mass: number) {
  let sum = 0;
  let lastMass = mass;

  do {
    const massToAdd = Math.max(calculateFuelFromWeight(lastMass), 0);
    sum += massToAdd;
    lastMass = massToAdd;
  } while(lastMass > 0);

  return sum;
}

function calculateFuelFromWeight(mass: number) {
  return Math.floor(mass / 3) - 2;
}
