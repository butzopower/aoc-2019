import * as fs from "fs";
import path from "path";
import { FuelCounter } from "../../lib/fuel-counter";

function main() {
  const filePath = path.join(__dirname, 'input.txt');
  const masses = fs.readFileSync(filePath, 'utf-8')
    .split('\n')
    .filter(x => x !== "")
    .map(x => Number(x));

  const totalFuel = new FuelCounter().count(masses);

  console.log(`Total Fuel: ${totalFuel}`);
}

main();
