import * as fs from "fs";
import * as readlineSync from "readline-sync";
import path from "path";
import { InputReader, IntCodeInterpreter, OutputWriter } from "../../lib/int-code-interpreter";

function main() {
  const filePath = path.join(__dirname, 'program.txt');
  const program = fs.readFileSync(filePath, 'utf-8');

  const interpreter = new IntCodeInterpreter(
    new ConsoleInputReader(),
    new ConsoleOutputWriter()
  );

  interpreter.load(program);

  interpreter.run();
}

class ConsoleInputReader implements InputReader {
  read(): number {
    const input = readlineSync.question('>');
    return Number(input);
  }
}

class ConsoleOutputWriter implements OutputWriter {
  write(output: number): void {
    console.log(output);
  }
}

main();
