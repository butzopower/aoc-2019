import { FuelCounter } from "../src/lib/fuel-counter";
import { expect } from "chai";

describe('fuel counter', () => {
  const subject = new FuelCounter();

  it('determines the fuel for a single entry', () => {
    expect(subject.count([12])).to.equal(2);
    expect(subject.count([14])).to.equal(2);
    expect(subject.count([1969])).to.equal(966);
    expect(subject.count([100756])).to.equal(50346);
  });

  it('determines the fuel for a batch of entries', () => {
    expect(subject.count([12, 12])).to.equal(2 * 2);
    expect(subject.count([12, 12, 12, 12])).to.equal(2 * 4);
    expect(subject.count([14, 14, 14, 14])).to.equal(2 * 4);
  });
});
